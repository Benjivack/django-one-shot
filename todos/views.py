from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list}
    return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    context = {"todo_list_detail": todo_list_detail}
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)
    context = {"form": form}
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/items/create.html", context)


def todo_item_edit(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = TodoItemForm(instance=list)
    context = {"form": form}
    return render(request, "todos/items/edit.html", context)
